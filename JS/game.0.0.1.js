// CHARACTER
var player = function(context, x, y, color, velocity){
    this.context = context;
    this.position = new Point(x, y);
    this.color = color;
    this.width = 30;
    this.height = 30;
    this.velocity = velocity;
};

// Draw the player at the current position on the screen
player.prototype.draw = function(){
    this.context.fillStyle = this.color;
    this.context.fillRect(
        this.position.x,
        this.position.y,
        this.width,
        this.height
    );
}

player.prototype.move = function(){
    this.position.x += 
}

/* A point object */
var Point = function(x, y)
{
	this.x = x;
	this.y = y;
}

/* A bounds object*/
var Bounds = function(x, y, width, height)
{
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
}

// MAP
var map = function(canvas, width, height){
	this.tile_size = 16;
    this.width = width;
    this.height = height;
	this.canvas = canvas;
};

map.prototype.draw_tile = function (x, y, tileColor, context) {
   	
};

map.prototype.draw_map = function(context){
    

};

map.prototype.draw = function (context) {
    this.draw_map(context);
};

window.requestAnimFrame =
  window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.oRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  function(callback) {
    return window.setTimeout(callback, 1000 / 60);
  };

var canvas = $('#gameCanvas')[0],
    ctx = canvas.getContext('2d');

canvas.width = 1024;
canvas.height = 768;

$('body').on('contextmenu', '#gameCanvas', function(e){ return false; });
canvas.addEventListener('keypress', doKeyDown, true);

var player = new player(ctx, 512, 384, "#FF5522", 10);

function doKeyDown(e){
    if ( e.keyCode == 119 ) {
        player.position.y -= 1;
    }

    if ( e.keyCode == 115 ) {
        player.position.y += 1;
    }

    if ( e.keyCode == 97 ) {
        player.position.x -= 1;
    }

    if ( e.keyCode == 100 ) {
        player.position.x += 1;
    }
}

var Loop = function() {
  
  ctx.fillStyle = '#333';
  ctx.fillRect(0, 0, canvas.width, canvas.height);

  player.draw(ctx);
  
  window.requestAnimFrame(Loop);
};

Loop();